class AddSuspendedToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :suspended, :boolean, default: false
  end
end
