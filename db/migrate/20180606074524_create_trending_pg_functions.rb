class CreateTrendingPgFunctions < ActiveRecord::Migration[5.2]
  def up
    execute %{
      CREATE OR REPLACE FUNCTION popularity(count integer, weight integer default 3) RETURNS integer AS $$
        SELECT count * weight
      $$ LANGUAGE SQL IMMUTABLE;
    }

    execute %{
      CREATE OR REPLACE FUNCTION recentness(stamp timestamp, sys_epoch integer default 1388380757) RETURNS integer AS $$
        SELECT ((EXTRACT(EPOCH FROM stamp) - sys_epoch) / 3600)::integer
      $$ LANGUAGE SQL IMMUTABLE;
    }

    execute %{
      CREATE FUNCTION ranking(counts integer, stamp timestamp, weight integer default 3) RETURNS integer AS $$
        SELECT popularity(counts, weight) + recentness(stamp)
      $$ LANGUAGE SQL IMMUTABLE;
    }
  end

  def down
    execute 'drop function popularity(integer, integer) cascade'
    execute 'drop function recentness(timestamp, integer) cascade'
    execute 'drop function ranking(integer, timestamp, integer) cascade'
  end
end
