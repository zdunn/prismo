class AddModifiedAtAndModifiedCountToStories < ActiveRecord::Migration[5.2]
  def change
    add_column :stories, :modified_at, :datetime
    add_column :stories, :modified_count, :integer, default: 0
  end
end
