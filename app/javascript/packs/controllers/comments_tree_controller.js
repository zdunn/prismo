import BaseController from './base_controller'
import SmoothScroll from 'smooth-scroll'
import cable from '../lib/actioncable'

const smoothScroll = new SmoothScroll()

export default class extends BaseController {
  static targets = ['rootComments', 'newReplyToastTpl']

  connect () {
    this.eventBus.addEventListener('comments:created', function (payload) {
      this.handleCommentsCreated(payload)
    }.bind(this))

    this.eventBus.addEventListener('comments:updated', function (payload) {
      this.handleCommentsUpdated(payload)
    }.bind(this))

    this._setupCable()
  }

  disconnect () {
    this.eventBus.removeEventListener('comments:created', function (payload) {
      this.handleCommentsCreated(payload)
    }.bind(this))

    this.eventBus.removeEventListener('comments:updated', function (payload) {
      this.handleCommentsUpdated(payload)
    }.bind(this))

    this._removeCable()
  }

  handleCommentsCreated({ detail }) {
    let newComment = detail.response[0].getElementsByClassName('comment')[0],
        newCommentId = newComment.getAttribute('data-id'),
        newCommentParentId = newComment.getAttribute('data-parent-id')

    // If data-parent-id is present, choose parent comment by this id. If not,
    // choose root comments container
    let parentCommentContainerEl = newCommentParentId ?
      this._findComentChildrenElById(newCommentParentId) :
      this.rootCommentsTarget

    // Append comment at the end of comments tree
    parentCommentContainerEl.appendChild(newComment)

    // Scroll to new comment
    this._scrollToCommentById(newCommentId)
  }

  handleCommentsUpdated ({ detail }) {
    let comment = detail.response[0].getElementsByClassName('comment')[0],
        commentId = comment.getAttribute('data-id'),
        existingComment = this._findCommentElById(commentId)

    // Update comment node
    existingComment.outerHTML = comment.outerHTML

    // Scroll to comment
    this._scrollToCommentById(commentId)
  }

  _setupCable () {
    let _this = this

    cable.subscriptions.create('UpdatesChannel', {
      received(payload) {
        if (payload.event == 'comments.created' && payload.data.account_id != _this.currentAccountId) {
          // Copy new reply template and append it to comment childrens list
          // Also: remove d-none class and setup data attributes
          const parentCommentChildren = _this.element.querySelector(`.comment[data-id="${payload.data.parent_id}"] .comment-children`),
                newReplyTpl = _this.newReplyToastTplTarget.cloneNode(true),
                res = parentCommentChildren.appendChild(newReplyTpl),
                showCommentLink = res.querySelector('a')

          showCommentLink.setAttribute('data-comment-id', payload.data.id)
          showCommentLink.setAttribute('data-action-path', payload.data.path)
          res.classList.remove('d-none')
        }
      }
    })
  }

  _removeCable () {
    cable.subscriptions.remove(cable.subscriptions.subscriptions[0])
  }

  _scrollToCommentById (id) {
    setTimeout(() => {
      let comment = this._findCommentElById(id)
      smoothScroll.animateScroll(comment, null, {
        offset: () => (window.innerHeight / 2)
      })
    }, 100)
  }

  _findCommentElById (id) {
    return this.element.querySelector(`[data-id="${id}"]`)
  }

  _findComentChildrenElById (id) {
    return this._findCommentElById(id).querySelector('.comment-children')
  }
}
