class ActivityPub::AcceptFollowSerializer < ActivityPub::BaseSerializer
  def data
    {
      id: id,
      type: 'Accept',
      actor: ActivityPub::TagManager.instance.uri_for(object.target_account),
      object: ActivityPub::FollowSerializer.new(object).data
    }
  end

  def id
    [
      ActivityPub::TagManager.instance.uri_for(object.target_account),
      '#accepts/follows/',
      object.id
    ].join
  end
end
