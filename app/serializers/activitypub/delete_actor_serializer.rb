class ActivityPub::DeleteActorSerializer < ActivityPub::BaseSerializer
  def data
    {
      id: ActivityPub::TagManager.instance.uri_for(object),
      type: 'Delete',
      actor: ActivityPub::TagManager.instance.uri_for(object),
      to: [ActivityPub::TagManager::COLLECTIONS[:public]],
      object: parent_object
    }
  end

  private

  def parent_object
    ActivityPub::ActorSerializer.new(object).data
  end
end
