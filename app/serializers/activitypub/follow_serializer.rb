class ActivityPub::FollowSerializer < ActivityPub::BaseSerializer
  def data
    {
      id: id,
      type: 'Follow',
      actor: ActivityPub::TagManager.instance.uri_for(object.account),
      object: ActivityPub::TagManager.instance.uri_for(object.target_account)
    }
  end

  def id
    ActivityPub::TagManager.instance.uri_for(object) ||
      [ActivityPub::TagManager.instance.uri_for(object.account), '#follows/', object.id].join
  end
end
