class ActivityPub::Activity::Follow < ActivityPub::Activity
  def perform
    target_account = account_from_uri(object_uri)

    return if target_account.nil? || !target_account.local? || @account.requested_follow?(target_account)

    # Fast-forward repeat follow requests
    if @account.following?(target_account)
      AuthorizeFollowService.new.call(@account, target_account, skip_follow_request: true, follow_request_uri: @json['id'])
      return
    end

    follow_request = FollowRequest.create!(account: @account, target_account: target_account, uri: @json['id'])

    if target_account.locked?
      CreateNotificationJob.call('follow_requested', author: @account, recipient: target_account, notifable: follow_request)
    else
      AuthorizeFollowService.new.call(@account, target_account)
      CreateNotificationJob.call('new_follower', author: @account, recipient: target_account)
    end
  end
end
