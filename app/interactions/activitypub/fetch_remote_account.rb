class ActivityPub::FetchRemoteAccount < ActiveInteraction::Base
  include JsonLdHelper

  SUPPORTED_TYPES = %w(Application Group Organization Person Service).freeze

  interface :uri
  boolean :id, default: true
  interface :prefetched_body, default: nil
  boolean :break_on_redirect, default: false

  # Should be called when uri has already been checked for locality
  # Does a WebFinger roundtrip on each call
  def execute
    @json = if prefetched_body.nil?
              fetch_resource(uri, id)
            else
              body_to_json(prefetched_body)
            end

    return if !supported_context? || !expected_type? || (break_on_redirect && @json['movedTo'].present?)

    @uri      = @json['id']
    @username = @json['preferredUsername']
    @domain   = Addressable::URI.parse(@uri).normalized_host

    return unless verified_webfinger?

    ActivityPub::ProcessAccount.run!(username: @username, domain: @domain, json: @json)
  rescue Oj::ParseError
    nil
  end

  private

  def verified_webfinger?
    webfinger                            = Goldfinger.finger("acct:#{@username}@#{@domain}")
    confirmed_username, confirmed_domain = split_acct(webfinger.subject)

    return webfinger.link('self')&.href == @uri if @username.casecmp(confirmed_username).zero? && @domain.casecmp(confirmed_domain).zero?

    webfinger                            = Goldfinger.finger("acct:#{confirmed_username}@#{confirmed_domain}")
    @username, @domain                   = split_acct(webfinger.subject)
    self_reference                       = webfinger.link('self')

    return false unless @username.casecmp(confirmed_username).zero? && @domain.casecmp(confirmed_domain).zero?
    return false if self_reference&.href != @uri

    true
  rescue Goldfinger::Error
    false
  end

  def split_acct(acct)
    acct.gsub(/\Aacct:/, '').split('@')
  end

  def supported_context?
    super(@json)
  end

  def expected_type?
    equals_or_includes_any?(@json['type'], SUPPORTED_TYPES)
  end
end
