class Flags::Create < ActiveInteraction::Base
  string :summary
  interface :flaggable
  interface :actor

  validates :summary, presence: true

  def execute
    flag = Flag.new
    flag.summary = summary
    flag.actor = actor
    flag.flaggable = flaggable

    if flag.save
      Notifications::SendForFlag.run!(flag: flag)
    else
      errors.merge!(flag.errors)
    end

    flag
  end
end
