class StoryPolicy < ApplicationPolicy
  def index?
    true
  end

  def create?
    user.present? && !user.account.silenced? && !user.account.suspended?
  end

  def edit?
    user.present? && (user.is_admin? || user == record.account.user)
  end

  def update?
    edit?
  end

  def update_title?
    # First check if user is admin
    return true if user.is_admin?

    # Then check if limit is not disabled
    limit = Setting.story_title_update_time_limit.to_i
    return true if limit.zero? && edit?

    diff = ((Time.current - record.created_at) / 1.minute).to_i

    edit? && diff < limit
  end

  def scrap?
    user.present? && (user.is_admin? || user == record.account.user)
  end

  def comment?
    user.present? && !user.account.silenced? && !user.account.suspended?
  end

  def toggle_like?
    user.present? &&
      !record.removed? &&
      !user.account.silenced? && !user.account.suspended?
  end

  def destroy?
    user.present? && (user == record.account&.user || user.is_admin?)
  end
end
