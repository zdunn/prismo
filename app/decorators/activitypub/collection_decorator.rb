class ActivityPub::CollectionDecorator
  attr_accessor :id, :type, :size, :items, :part_of, :first, :last, :next, :prev

  def initialize(opts = {})
    @id = opts[:id]
    @type = opts[:type]
    @size = opts[:size]
    @items = opts[:items]
    @part_of = opts[:part_of]
    @first = opts[:first]
    @last = opts[:last]
    @next = opts[:next]
    @prev = opts[:prev]
  end
end
