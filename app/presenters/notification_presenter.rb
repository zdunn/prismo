class NotificationPresenter < Draper::Decorator
  delegate_all

  def notifable_path
    object.notifable.decorate.id
  end

  def comment_story_path
    object.notifable.story.decorate.path
  end

  def sentence
    author = object.author.decorate
    notifable = object.notifable&.decorate
    context = object.context&.decorate

    params = {
      author: author,
      author_url: author.path
    }

    case object.event_key.to_sym
    when :comment_reply
      params.merge!(notifable_url: notifable.path,
                    context_url: context.path,
                    context: context.title)

    when :story_reply
      params.merge!(notifable: notifable.to_s,
                    notifable_url: notifable.path,
                    context: context&.title,
                    context_url: context&.path)

    when :new_flag
      params.merge!(notifable: notifable.to_s,
                    notifable_url: notifable.path,
                    context: context&.to_flag_title,
                    context_url: context&.path)
    end

    h.t("notifications.#{object.event_key}.to_s", params).html_safe
  end
end
