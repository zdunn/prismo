class SilenceAccountService
  def call(account)
    account.update(silenced: true)
  end
end
