module Admin
  class AccountsController < BaseController
    def index
      authorize Account

      @accounts = Account.page(params[:page])
    end

    def show
      @account = find_account
      authorize @account

      @user = @account.user
    end

    def suspend
      @account = find_account
      authorize @account

      Accounts::SuspendJob.perform_later(@account.id)

      redirect_back(
        fallback_location: admin_accounts_path,
        notice: 'Account sheduled for suspension'
      )
    end

    def silence
      @account = find_account
      authorize @account

      SilenceAccountService.new.call(@account)

      redirect_back(
        fallback_location: admin_accounts_path,
        notice: 'Account silenced'
      )
    end

    def unsilence
      @account = find_account
      authorize @account

      @account.update(silenced: false)

      redirect_back(
        fallback_location: admin_accounts_path,
        notice: 'Account unsilenced'
      )
    end

    private

    def find_account
      Account.find(params[:id])
    end
  end
end
