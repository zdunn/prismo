module Admin
  class DomainBlocksController < ApplicationController
    layout 'settings'

    def index
      authorize ActivityPub::DomainBlock
      @domain_blocks = ActivityPub::DomainBlock.all
    end

    def new
      authorize ActivityPub::DomainBlock
      @domain_block = ActivityPub::DomainBlocks::Create.new
    end

    def create
      authorize ActivityPub::DomainBlock
      @domain_block = ActivityPub::DomainBlocks::Create.run(domain_block_params)

      if @domain_block.valid?
        redirect_to admin_domain_blocks_path, notice: 'Domain successfully blocked'
      else
        render :new
      end
    end

    def destroy
      domain_block = ActivityPub::DomainBlock.find(params[:id])
      authorize domain_block

      domain_block.destroy!

      redirect_to admin_domain_blocks_path, notice: 'Domain successfully unblocked'
    end

    private

    def domain_block_params
      params.require(:activitypub_domain_block).permit(:domain, :severity)
    end
  end
end
