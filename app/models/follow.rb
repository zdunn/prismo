class Follow < ApplicationRecord
  belongs_to :account, counter_cache: :following_count
  belongs_to :target_account, class_name: 'Account', counter_cache: :followers_count
end
