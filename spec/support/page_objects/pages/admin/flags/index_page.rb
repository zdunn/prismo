module Admin
  module Flags
    class IndexPage < SitePrism::Page
      set_url '/admin/flags'
      set_url_matcher %r{\/admin\/flags\z}

      elements :flags, '[data-cap="flag"]'
    end
  end
end
