require_relative '../../../sections/navbar_section'
require_relative '../../../sections/story_row_section'

class DashboardFollowedAccountsPage < SitePrism::Page
  set_url '/dashboard/followed_accounts'
  set_url_matcher %r{\/dashboard(\/followed_accounts)?\z}

  section :navbar, ::NavbarSection, '.navbar-main'
  sections :stories, ::StoryRowSection, '.stories-list li.story-row'
end
