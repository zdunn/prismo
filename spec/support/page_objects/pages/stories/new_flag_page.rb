class Stories::NewFlagPage < SitePrism::Page
  set_url '/posts{/id}/flag/new'
  set_url_matcher %r{\/posts\/\d+\/flag\/new\z}

  section :new_flag_form, '#new_flag' do
    element :flag_summary_input, '#flag_summary'
    element :submit_btn, 'input[type="submit"]'

    def submit
      submit_btn.click
    end
  end
end
