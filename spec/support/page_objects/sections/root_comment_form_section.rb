require_relative 'new_comment_form_section'

class RootCommentFormSection < SitePrism::Section
  element :no_comments_yet, '.empty'
  element :add_first_comment_btn, 'button', text: 'ADD FIRST COMMENT'
  section :new_comment_form, ::NewCommentFormSection, 'form#new_comment'
end
