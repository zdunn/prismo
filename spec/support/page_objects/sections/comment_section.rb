require_relative 'new_comment_form_section'
require_relative 'edit_comment_form_section'

class CommentSection < SitePrism::Section
  element :reply_btn, '.comment-reply-btn'
  element :edit_btn, '.comment-edit-btn'
  element :like_button, '.comment-like-btn'
  element :likes_count, '.comment-like-btn'
  element :body, '.comment-body'

  section :new_comment_form, ::NewCommentFormSection, '.new_comment'
  section :edit_comment_form, ::EditCommentFormSection, 'form[data-mode="update"]'
  sections :child_comments, ::CommentSection, '.comment'

  def click_like_button
    like_button.click
  end

  def liked?
    root_element[:'data-liked'] == 'true'
  end
end
