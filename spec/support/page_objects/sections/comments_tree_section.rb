require_relative 'root_comment_form_section'
require_relative 'comment_section'

class CommentsTreeSection < SitePrism::Section
  section :root_comment_form, ::RootCommentFormSection, '.root-comment-form'
  sections :comments, ::CommentSection, '.comment'
end
