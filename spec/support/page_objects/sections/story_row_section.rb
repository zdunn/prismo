class StoryRowSection < SitePrism::Section
  element :root, '.story-row'
  element :like_button, '.story-row-score-btn'
  element :likes_count, '.story-row-score-likes-count'
  element :more_link, 'a', text: 'more'
  element :flag_link, 'a', text: 'Flag'

  def click_like_button
    like_button.click
  end

  def liked?
    root_element[:'data-liked'] == 'true'
  end

  def open_more_dropdown
    more_link.click
  end
end
