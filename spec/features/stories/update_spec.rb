require 'rails_helper'

feature 'Updating story' do
  let(:edit_story_page) { Stories::EditPage.new }
  let(:story_page) { Stories::ShowPage.new }
  let(:sign_in_page) { SignInPage.new }

  let(:user) { create(:user, :with_account, password: 'TestPass') }
  let!(:supergroup) { create(:group, :super) }
  let(:story) { create(:story, :link, description: 'Some description', account: user.account) }

  def sign_user_in
    sign_in_page.load
    sign_in_page.sign_in_using(user.email, 'TestPass')
  end

  before do
    stub_request(:get, 'https://example.com/')
      .to_return(status: 200, body: '', headers: {})
  end

  scenario 'regular user tries to remove the description from link story', js: true do
    sign_user_in
    edit_story_page.load(id: story.id)

    expect(edit_story_page).to be_displayed

    edit_story_page.description_field.set ''
    edit_story_page.submit_button.click

    expect(story_page).to be_displayed
    expect(story_page).to have_content 'Story has been updated'
  end
end
