require 'rails_helper'

describe Comments::Create do
  let(:inputs) { valid_inputs }
  let(:account) { create(:account) }
  let!(:group) { create(:group, supergroup: true) }
  let(:story) { create(:story) }
  let(:valid_inputs) do
    {
      url: 'https://example.com/@mb/100742981773257229',
      uri: 'https://example.com/users/mb/statuses/100742981773257229',
      story_id: story.id,
      body: 'Sample body',
      created_at: Time.now.to_s,
      account: account,
      local: false,
      auto_like: false
    }
  end

  let(:outcome) { described_class.run(inputs) }
  let(:outcome!) { described_class.run!(inputs) }
  let(:result) { outcome.result }
  let(:errors) { outcome.errors }

  describe '#run' do
    subject { outcome }

    it { expect { subject }.to change(Comment, :count).by(1) }

    it 'assigns all given attributes' do
      expect(result.url).to eq inputs[:url]
      expect(result.uri).to eq inputs[:uri]
      expect(result.story_id).to eq inputs[:story_id]
      expect(result.body).to eq inputs[:body]
      expect(result.created_at.to_i).to eq Time.parse(inputs[:created_at]).to_i
      expect(result.account).to eq inputs[:account]
      expect(result.local).to eq inputs[:local]
    end

    it 'assigns domain from uri' do
      expect(result.domain).to eq 'example.com'
    end

    it 'broadcast event creation' do
      allow(Comments::BroadcastCreation).to receive(:run!)
      subject
      expect(Comments::BroadcastCreation).to have_received(:run!).with(comment: Comment.last)
    end

    it 'calls #cache_depth' do
      allow_any_instance_of(Comment).to receive(:cache_depth)
      outcome = subject
      expect(outcome.result).to have_received(:cache_depth)
    end

    it 'calls #cache_body' do
      allow_any_instance_of(Comment).to receive(:cache_body)
      outcome = subject
      expect(outcome.result).to have_received(:cache_body)
    end

    context 'when auto_like is true' do
      let(:inputs) { valid_inputs.merge(auto_like: true) }

      it { expect { subject }.to change(Like, :count).by(1) }

      it 'creates a like with proper associations' do
        outcome = subject
        like = Like.last

        expect(like.likeable).to eq outcome.result
        expect(like.account).to eq outcome.result.account
      end
    end

    context 'when auto_like is false' do
      let(:inputs) { valid_inputs.merge(auto_like: false) }

      it { expect { subject }.to_not change(Like, :count) }
    end

    context 'when comment is local' do
      let(:inputs) { valid_inputs.merge(local: true) }

      it 'distributes it' do
        expect(ActivityPub::DistributionJob).to receive(:call)
        subject
      end
    end

    context 'when comment is remote' do
      let(:inputs) { valid_inputs.merge(local: false) }

      it 'does not distribute it' do
        expect(ActivityPub::DistributionJob).to_not receive(:call)
        subject
      end
    end

    context 'when body is empty' do
      let(:inputs) { valid_inputs.merge(body: '') }

      it { is_expected.to be_invalid }
    end
  end
end
