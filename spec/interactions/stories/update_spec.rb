require 'rails_helper'

describe Stories::Update do
  let(:user) { create(:user, :with_account) }
  let(:story) { create(:story, :link, account: user.account) }

  let(:args) do
    {
      account: user.account,
      title: 'Sample title',
      tag_list: 'foo, bar',
      story: story,
      url: story.url,
      description: story.description
    }
  end

  describe '#run' do
    subject { described_class.run(args) }

    context 'when params are fully valid' do
      it { is_expected.to be_valid }

      it 'updates modified_at column' do
        expect { subject }.to change(story, :modified_at)
      end

      it 'updates modified_count column' do
        expect { subject }.to change(story, :modified_count).by(1)
      end
    end

    context 'when actor is admin' do
      let(:user) { create(:user, :with_account, :admin) }

      it 'is possible to update story url' do
        args[:url] = 'https://changed.com'
        expect { subject }.to change(story, :url).to 'https://changed.com'
      end

      it 'is possible to update title even when title update limit is exceed' do
        args[:title] = 'Changed title'
        expect { subject }.to change(story, :title).to 'Changed title'
      end
    end

    context 'when actor is a regular user' do
      it 'is not possible to update story url' do
        args[:url] = 'https://changed.com'
        expect { subject }.to_not change(story, :url)
      end

      it 'is not possible to update title when title update limit is exceed' do
        story.update(created_at: 65.minutes.ago)
        args[:title] = 'Changed title'

        expect(subject).to_not be_valid
      end
    end
  end
end
