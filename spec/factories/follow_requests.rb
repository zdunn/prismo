FactoryBot.define do
  factory :follow_request do
    account
    association :target_account, factory: :account
  end
end
