require 'rails_helper'

describe ActivityPub::DeleteSerializer do
  let(:story) { create(:story) }

  describe '#as_json' do
    it 'does not raise exception' do
      expect { described_class.new(story) }.to_not raise_exception
    end
  end
end
